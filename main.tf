resource "google_compute_instance" "haproxy" {
  name         = "haproxy"
  machine_type = "n1-standard-1"
  zone         = "asia-southeast1-b"

  tags = ["haproxy"]

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }

  // Local SSD disk
  scratch_disk {}

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

}
